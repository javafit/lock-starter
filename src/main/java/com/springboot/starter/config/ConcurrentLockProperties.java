package com.springboot.starter.config;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("concurrent.lock")
public class ConcurrentLockProperties {

    private String lockType;

    private Integer defaultlockTime;

    private Integer retryNum;

    public String getLockType() {
        return lockType;
    }

    public void setLockType(String lockType) {
        this.lockType = lockType;
    }

    public Integer getDefaultlockTime() {
        return defaultlockTime;
    }

    public void setDefaultlockTime(Integer defaultlockTime) {
        this.defaultlockTime = defaultlockTime;
    }

    public Integer getRetryNum() {
        return retryNum;
    }

    public void setRetryNum(Integer retryNum) {
        this.retryNum = retryNum;
    }
}
