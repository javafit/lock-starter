package com.springboot.starter.concurrentlock;

import com.springboot.starter.config.ConcurrentLockProperties;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

public class DbLockService implements ILockService {


    @Autowired
    private DataSource dataSource;


    @Autowired
    private ConcurrentLockProperties properties;

    private SqlSessionTemplate template;

    @PostConstruct
    private void init() throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/*.xml"));
        template = new SqlSessionTemplate(bean.getObject());
    }




    @Override
    public void lock() {

    }

    @Override
    public void unlock() {

    }
}
