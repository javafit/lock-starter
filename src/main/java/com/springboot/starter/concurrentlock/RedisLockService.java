package com.springboot.starter.concurrentlock;

import com.springboot.starter.config.ConcurrentLockProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

public class RedisLockService implements ILockService {


    @Autowired
    private StringRedisTemplate template;

    @Autowired
    private ConcurrentLockProperties properties;

    @Override
    public void lock() {

    }

    @Override
    public void unlock() {

    }
}
