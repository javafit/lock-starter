package com.springboot.starter.concurrentlock;

import com.springboot.starter.config.ConcurrentLockProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import javax.annotation.PostConstruct;

public class ConcurrentLockService {


    private static Logger logger = LoggerFactory.getLogger(ConcurrentLockService.class);

    private ILockService lockService;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ConcurrentLockProperties properties;

    @PostConstruct
    private void init(){
        if(properties.getLockType().equalsIgnoreCase("database")){
            lockService=context.getAutowireCapableBeanFactory().createBean(DbLockService.class);
        }
        if(properties.getLockType().equalsIgnoreCase("redis")){
            lockService=context.getAutowireCapableBeanFactory().createBean(RedisLockService.class);
        }
        if(null==lockService){
            logger.error("lockType error. like [database,redis]");
        }
    }




    public void lock(){
        lockService.lock();
    }

    public void unlock(){
        lockService.unlock();
    }

}
