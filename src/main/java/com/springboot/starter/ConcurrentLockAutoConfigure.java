package com.springboot.starter;


import com.springboot.starter.concurrentlock.ConcurrentLockService;
import com.springboot.starter.config.ConcurrentLockProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(ConcurrentLockService.class)
@EnableConfigurationProperties(ConcurrentLockProperties.class)
public class ConcurrentLockAutoConfigure {

    private final ConcurrentLockProperties properties;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    public ConcurrentLockAutoConfigure(ConcurrentLockProperties properties) {
        this.properties = properties;
    }


    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "concurrent.lock",havingValue = "true")
    public ConcurrentLockService concurrentLockService (){
        return ctx.getAutowireCapableBeanFactory().createBean(ConcurrentLockService.class);
    }
}
